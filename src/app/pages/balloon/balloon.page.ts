import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-balloon",
  templateUrl: "./balloon.page.html",
  styleUrls: ["./balloon.page.scss"],
})
export class BalloonPage implements OnInit {
  public selected: boolean = false;
  constructor() {}

  ngOnInit() {}

  onClickHandler(): void {
    this.selected = !this.selected;
  }
}
