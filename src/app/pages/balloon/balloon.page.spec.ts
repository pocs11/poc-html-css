import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BalloonPage } from './balloon.page';

describe('BalloonPage', () => {
  let component: BalloonPage;
  let fixture: ComponentFixture<BalloonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalloonPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BalloonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
